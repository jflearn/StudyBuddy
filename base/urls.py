# -*- coding: utf-8 -*-
# @Time     : 2022/6/21 19:25
# @Author   : JustFly
# @File     : urls.py
# @Software : PyCharm

from django.urls import path
from . import views

urlpatterns = [
    # 定义路由
    path('', views.home, name="home"),
    path('room/<str:pk>/', views.room, name="room"),
    path('create-room/', views.create_room, name="create-room"),
    path('update-room/<str:pk>', views.update_room, name="update-room"),
    path('delete-room/<str:pk>', views.delete_room, name="delete-room"),

    path('delete-message/<str:pk>', views.delete_message, name="delete-message"),

    path('login/', views.login_page, name="login"),
    path('logout/', views.logout_user, name="logout"),

    path('register/', views.register_page, name="register"),

    path('user-profile/<str:pk>/', views.user_profile, name="user-profile"),
    path('update-user/', views.update_user, name="update-user"),

    path('mb-topics-page/', views.mb_topics_page, name="mb-topics-page"),
    path('mb-activity-page/', views.mb_activity_page, name="mb-activity-page"),


]
