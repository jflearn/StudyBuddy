# -*- coding: utf-8 -*-
# @Time     : 2022/6/22 15:59
# @Author   : JustFly
# @File     : forms.py
# @Software : PyCharm

# 魔法引擎模型 创建表单
from django.forms import ModelForm
from .models import Room, User

# 配置头像
from django.contrib.auth.forms import UserCreationForm

# 友好展示表单提示信息
from django import forms

# 注册账号使用
class MyUserCreationForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']

        # labels = {
        #     'username': '昵称',
        #     'email': '邮箱',
        #     'password1': '密码',
        #     'password2': '确认密码',
        # }



class RoomForm(ModelForm):
    class Meta:
        model = Room
        fields = '__all__'  # 全部字段 # 指定字段用 ['name', 'host', ...]
        exclude = ['host', 'participants']


# 编辑账号使用
class UserForm(ModelForm):

    class Meta:
        model = User
        fields = ['avatar', 'username', 'email', 'bio']

        labels = {
            'avatar': '头像',
            'username': '昵称',
            'email': '邮箱',
            'bio': '个性签名',
        }
