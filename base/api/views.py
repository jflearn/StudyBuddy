# -*- coding: utf-8 -*-
# @Time     : 2022/6/24 12:09
# @Author   : JustFly
# @File     : views.py
# @Software : PyCharm


from rest_framework.decorators import api_view
from rest_framework.response import Response

from base.models import Room, Message, Topic, User
# from django.contrib.auth.models import User
from .serializers import RoomSerializer, MessageSerializer, TopicSerializer, UserSerializer


@api_view(['GET'])
def getRouts(request):
    routes = [
        'GET /api',

        'GET /api/rooms',
        'GET /api/rooms/:id',

        'GET /api/messages',
        'GET /api/messages/:id',

        'GET /api/topics',
        'GET /api/topics/:id',

        'GET /api/users',
        'GET /api/users/:id',
    ]

    return Response(routes)


# 获取所有对象
@api_view(['GET'])
def getRooms(request):

    rooms = Room.objects.all()
    serializers = RoomSerializer(rooms, many=True)  # many=True 代表rooms中有多个对象
    return Response(serializers.data)


@api_view(['GET'])
def getMessages(request):

    room_messages = Message.objects.all()
    serializers = MessageSerializer(room_messages, many=True)
    return Response(serializers.data)


@api_view(['GET'])
def getTopics(request):

    topics = Topic.objects.all()
    serializers = TopicSerializer(topics, many=True)
    return Response(serializers.data)


@api_view(['GET'])
def getUsers(request):

    users = User.objects.all()
    serializers = UserSerializer(users, many=True)
    return Response(serializers.data)


# 获取某个对象
@api_view(['GET'])
def getRoom(request, pk):

    room_pk = Room.objects.get(id=pk)
    serializer = RoomSerializer(room_pk, many=False)  # many=True 代表rooms中有多个对象
    return Response(serializer.data)


@api_view(['GET'])
def getMessage(request, pk):

    message_pk = Message.objects.get(id=pk)
    serializer = MessageSerializer(message_pk, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def getTopic(request, pk):

    topic_pk = Topic.objects.get(id=pk)
    serializer = TopicSerializer(topic_pk, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def getUser(request, pk):

    user_pk = User.objects.get(id=pk)
    serializer = UserSerializer(user_pk, many=False)
    return Response(serializer.data)


