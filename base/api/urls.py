# -*- coding: utf-8 -*-
# @Time     : 2022/6/24 12:09
# @Author   : JustFly
# @File     : urls.py
# @Software : PyCharm


from django.urls import path
from . import views


urlpatterns = [
    path('', views.getRouts),
    path('rooms/', views.getRooms),
    path('messages/', views.getMessages),
    path('topics/', views.getTopics),
    path('users/', views.getUsers),

    path('rooms/<str:pk>/', views.getRoom),
    path('messages/<str:pk>/', views.getMessage),
    path('topics/<str:pk>/', views.getTopic),
    path('users/<str:pk>/', views.getUser),


]