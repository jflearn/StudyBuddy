# -*- coding: utf-8 -*-
# @Time     : 2022/6/24 12:09
# @Author   : JustFly
# @File     : serializers.py 序列化
# @Software : PyCharm

from rest_framework.serializers import ModelSerializer
from base.models import Room, Message, Topic, User
# from django.contrib.auth.models import User


class RoomSerializer(ModelSerializer):
    class Meta:
        model = Room
        fields = '__all__'


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class TopicSerializer(ModelSerializer):
    class Meta:
        model = Topic
        fields = '__all__'


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email']


