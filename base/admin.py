from django.contrib import admin


# 设置后台名称
admin.site.site_header = '学习伙伴管理后台'  # 设置header
admin.site.site_title = '学习伙伴管理后台'   # 设置title
admin.site.index_title = '学习伙伴管理后台'


# 引入模型
from .models import Room, Topic, Message, User, IP

# 让模型User注册到Django后台可视化管理
class UserAdmin(admin.ModelAdmin):
    #fields = ['question_text', 'pub_date']  # 列表里的顺序 表示后台的展示顺序
    fieldsets = [
        ('账号', {'fields': ['username', 'password', 'last_login']}),
        ('个人信息', {'fields': ['avatar', 'name', 'gender', 'email', 'bio']}),
        ('权限', {'fields': ['is_active',  'is_staff', 'is_superuser' ]}),
        # ('权限', {'fields': ['is_active',  'is_staff', 'is_superuser', 'groups', 'user_permissions',]}),
    ]
    list_display = ('username', 'name', 'gender', 'email', 'bio', 'last_login')
    # list_filter = ('groups', 'is_active')
    search_fields = ['username',  'name', 'email']

admin.site.register(User, UserAdmin)

# 让模型Room注册到Django后台可视化管理
class RoomAdmin(admin.ModelAdmin):
    # # 房主 主持人
    # host = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, verbose_name="创建者")
    # # 一对多关系 设置外键 # 级联删除 Topic被删则属于该Topic的所有Room被删 # 这里我们设置非级联删除 允许为空
    # topic = models.ForeignKey(Topic, on_delete=models.SET_NULL, null=True, verbose_name="主题分类")
    # name = models.CharField(max_length=200, verbose_name="名称")
    # desc = models.TextField(null=True, blank=True, verbose_name="描述")
    # participants = models.ManyToManyField(User, related_name='participants', blank=True, verbose_name="参与者")  # 多对多关系
    # updated = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    # created = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    #fields = ['question_text', 'pub_date']  # 列表里的顺序 表示后台的展示顺序
    fieldsets = [
        ('', {'fields': ['name', 'host', 'topic', 'desc', 'participants']}),
    ]
    list_display = ('name', 'host', 'topic', 'created')
    # list_filter = ('groups', 'is_active')
    search_fields = ['name', 'host', 'topic', 'desc',]

admin.site.register(Room, RoomAdmin)


# 让模型Topic注册到Django后台可视化管理
class TopicAdmin(admin.ModelAdmin):
    fieldsets = [
        ('', {'fields': ['name', 'room_count']}),
    ]
    list_display = ['name', 'room_count', 'created']
    search_fields = ['name']


admin.site.register(Topic, TopicAdmin)


# 让模型Message注册到Django后台可视化管理
class MessageAdmin(admin.ModelAdmin):
    #fields = ['question_text', 'pub_date']  # 列表里的顺序 表示后台的展示顺序
    fieldsets = [
        ('', {'fields': ['user', 'room', 'body']}),
    ]
    list_display = ('body', 'room', 'user', 'created')
    # list_filter = ('groups', 'is_active')
    search_fields = ['body', 'user', 'room']

admin.site.register(Message, MessageAdmin)

# 让模型IP注册到Django后台可视化管理
class IPAdmin(admin.ModelAdmin):
    fieldsets = [
        ('', {'fields': ['user',  'ip']}),
    ]

    list_display = ('user', 'created', 'ip')
    # list_filter = ('groups', 'is_active')
    search_fields = ['username']

admin.site.register(IP, IPAdmin)
