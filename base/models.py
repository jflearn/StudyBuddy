from django.db import models

# Create your models here.

# 引用Django的用户认证
# from django.contrib.auth.models import User

# 重整User模型
from django.contrib.auth.models import AbstractUser

# 学习伙伴 数据库表

# 重整User模型

class User(AbstractUser):
    GENDER = (
        ('1', '男'),
        ('2', '女'),
        ('0', '未知'),
    )
    name = models.CharField(max_length=200, null=True, verbose_name="昵称", blank=True)
    gender = models.CharField(max_length=1, choices=GENDER, verbose_name="性别", blank=True)
    email = models.EmailField(unique=True, null=True, verbose_name="邮箱", blank=True)
    bio = models.TextField(null=True, verbose_name="个性签名", blank=True)
    # 头像
    avatar = models.ImageField(null=True, default="avatar.svg", verbose_name="个人头像", blank=True)
    # USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "用户管理"
        verbose_name_plural = verbose_name



class Topic(models.Model):
    name = models.CharField(max_length=200, verbose_name="主题名称")
    room_count = models.IntegerField(default=0, null=True, verbose_name="房间总数")  # 统计相关房间数
    created = models.DateTimeField(auto_now_add=True, verbose_name="创建时间", null=True, blank=True)

    def __str__(self):
        return self.name

    # 配置打印顺序
    class Meta:
        verbose_name = "主题管理"
        verbose_name_plural = verbose_name
        ordering = ['-room_count']  # - 代表按属性降序


class IP(models.Model):
    id = models.IntegerField(auto_created=True, primary_key=True, verbose_name="房间 ID")
    ip = models.CharField(max_length=20, null=True, verbose_name="IP 地址")
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, verbose_name="用户")
    # updated = models.DateTimeField(auto_now=True, verbose_name="更新日期")
    created = models.DateTimeField(auto_now_add=True, verbose_name="登录时间")

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = "访问日志"
        verbose_name_plural = verbose_name


class Room(models.Model):
    # 房主 主持人
    host = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, verbose_name="创建者")
    # 一对多关系 设置外键 # 级联删除 Topic被删则属于该Topic的所有Room被删 # 这里我们设置非级联删除 允许为空
    topic = models.ForeignKey(Topic, on_delete=models.SET_NULL, null=True, verbose_name="主题分类")
    name = models.CharField(max_length=200, verbose_name="名称")
    desc = models.TextField(null=True, blank=True, verbose_name="描述")
    participants = models.ManyToManyField(User, related_name='participants', blank=True, verbose_name="参与者")  # 多对多关系
    updated = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    created = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    # 配置打印顺序
    class Meta:
        verbose_name = "房间管理"
        verbose_name_plural = verbose_name
        ordering = ['-created', '-updated']  # - 代表按属性降序

    def __str__(self):
        return self.name


class Message(models.Model):
    # 一对多关系 设置外键 # 级联删除 User被删则属于User的所有message被删
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="发送者")
    # 一对多关系 设置外键 # 级联删除 room被删则所有message被删
    room = models.ForeignKey(Room, on_delete=models.CASCADE, verbose_name="所属房间")
    body = models.TextField(verbose_name="消息内容")
    updated = models.DateTimeField(auto_now=True, verbose_name="更新时间")  # 上次编辑/更新时间
    created = models.DateTimeField(auto_now_add=True, verbose_name="发送时间")

    # 配置打印顺序
    class Meta:
        verbose_name = "消息管理"
        verbose_name_plural = verbose_name
        ordering = ['-created', '-updated']  # - 代表按属性降序

    def __str__(self):
        return self.body[0:50] + ' ......'  # 预览前50个字符...




