# StudyBuddy

####  :bowtie: 介绍
Django 学习伙伴项目。


####  :fa-book: 技术栈
Django、 rest_framework、SqlLite3 （Python3.9.1）
Web（JS、CSS、HTML）

####  :fa-diamond:  食用教程


```
python manage.py runserver
```



#### :fa-th-large: [功能预览](http://www.hugboy.wiki:8001/)

##### 后台
![输入图片说明](%E6%95%88%E6%9E%9C%E6%88%AA%E5%9B%BE/%E5%9B%BE%E7%89%87-%E9%A6%96%E9%A1%B5.png)

![输入图片说明](%E6%95%88%E6%9E%9C%E6%88%AA%E5%9B%BE/%E5%9B%BE%E7%89%87-%E5%90%8E%E5%8F%B0-%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.png)

##### 注册

 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/114802_d746b75e_9937562.png "屏幕截图.png")
 
##### 登录
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/114919_79dd850a_9937562.png "屏幕截图.png")
 
##### 设置个人信息
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/115258_bc820c0d_9937562.png "屏幕截图.png")
 
##### 主页
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/115405_7a035f08_9937562.png "屏幕截图.png")
 
##### 创建房间
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/115818_fe8f5e4b_9937562.png "屏幕截图.png")
 
##### 进入房间交流
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120005_5c72ebf4_9937562.png "屏幕截图.png")
 
##### 删除房间
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120053_76f706cd_9937562.png "屏幕截图.png")
 
##### 个人主页
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120137_fbb8f445_9937562.png "屏幕截图.png")
 
##### 检索主题
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120605_acf9e965_9937562.png "屏幕截图.png")
 
##### 按照主题/输入关键字 检索房间
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120321_43070617_9937562.png "屏幕截图.png")
 
##### 注销登录
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120714_0be383ef_9937562.png "屏幕截图.png")
 
 ![输入图片说明](https://images.gitee.com/uploads/images/2022/0625/120738_9000d511_9937562.png "屏幕截图.png")
 

##### 移动端适配

![输入图片说明](%E6%95%88%E6%9E%9C%E6%88%AA%E5%9B%BE/%E5%9B%BE%E7%89%87-%E6%89%8B%E6%9C%BA%E7%AB%AF%E9%80%82%E9%85%8D.png)

####  :fa-heart-o: 关于

此项目基于[国外Django大佬开源项目StudyBud](https://www.github.com/divanov11/StudyBud/)开发，仅供学习使用。




